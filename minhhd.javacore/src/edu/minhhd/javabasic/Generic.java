package edu.minhhd.javabasic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class Generic {
	//Generic cho phep chung ta quan ly viec them va get giu lieu cua Arraylist theo 1 loai duy nhat
	/**
	 * Vi ArrayList khi co nhieu doi tuong se kho kiem soat 
	 * va Generic se xu ly dieu do
	 */
	public static void main(String[]args){
		ArrayList<A> alist = new ArrayList<A>();
		alist.add(new A());
		alist.add(new A());
		for (int i = 0; i < alist.size(); i++) {
			alist.get(i).go();
		}
		
		HashMap<Integer , B> map = new HashMap<Integer ,B>();
		map.put(1, new B());
		map.put(2, new B());
		
		Vector<C> vector = new Vector<C>();
		vector.add(new C());
		vector.add(new D());
		//o day class D la class con cua class C nen generic vector C co the add duoc class D
	}
}

class A{
	public void go(){
		
	}
}

class B{
	public void show(){
		
	}
}

class C{
	
}

class D extends C{
	
}
