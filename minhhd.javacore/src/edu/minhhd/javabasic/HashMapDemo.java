package edu.minhhd.javabasic;
import java.util.HashMap;
public class HashMapDemo {
	public static void main(String[] args){
		
		// HashMap gan cac phan tu theo key qua phuong thuc put
			HashMap map = new HashMap();
			map.put("Object1", "QuanVanTruong");
			map.put("Object2", "TrieuTuLong");
			map.put("Object3", "TruongPhi");
			map.put(new Integer(5), "TrieuVu");
			
			System.out.println(map.get("Object3"));
			System.out.println(map.get(5));
			map.remove("Object3");
			// Xoa bo 1 key trong map
			
			map.clear();
			//Xoa bo toan bo key trong map
	}
}
