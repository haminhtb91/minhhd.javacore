package edu.minhhd.javabasic;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorDemo {
	public static void main(String [] args){
		ArrayList alist = new ArrayList();
		alist.add("Ha");
		alist.add("Duc");
		alist.add("Minh");
		//thay vi dung vong for de lay cac gia tri trong arraylist ta dung iterator
		Iterator iterator = alist.iterator();
		while(iterator.hasNext()){
			//hasnext tra ve boolean xem arraylist da het phan tu chua
			System.out.println(iterator.next());
		}
	}
}
