package edu.minhhd.javabasic;

public class ThreadDemo {
	public static void main(String [] args){
		Thread thread = new Thread(){
			public void run(){
				System.out.println("Pause");
				System.out.println("Resume");
			}
		};
		thread.run();
	}
}
