package edu.minhhd.javabasic;

public class TimeDemo {
	public static void main(String args[]){
		long start = System.currentTimeMillis();
		try {
			for (int i = 0; i < 1000; i++) {
				Thread.sleep(2);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();
		System.out.println("Thoi Gian thuc hien tien trinh  : " + (end - start));
	}
	// Cách 2 
	long start_nano = System.nanoTime();
	long end_nano = System.nanoTime();
	//cach tinh thoi gian he thong bang nanoTime co do chinh xac cao
}
